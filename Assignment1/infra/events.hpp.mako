
#ifndef ${guard}
#define ${guard}
#include <gtk/gtk.h>

namespace CG { namespace ui {
class Context {
    Context();
public:
%for control in controls:
    ${control.ptr_t} ${control.name};
%endfor
}

%for event in events:
static void ${event.handler}();
%endfor

}}
#endif // ${guard}


