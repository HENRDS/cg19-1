#!usr/bin/env python3
from xml.etree import ElementTree
from string import Template
import os
from mako.template import Template



class Control:
    def __init__(self, name, type_):
        self.name = name
        self.type_ = type_
        self.ptr_t = type_ + "*"

    def __repr__(self):
        return f"Control(name={self.name}, type={self.type_}, ptr_t={self.ptr_t})"

class Event:
    def __init__(self, ctrl: Control, name: str, handler: str):
        self.ctrl = ctrl
        self.name = name
        self.handler = handler

    def __repr__(self):
        return f"Event(ctrl={self.ctrl.name}, name={self.name}, handler={self.handler})"



def main():
    file = os.path.abspath( "../main_window.glade")
    tree = ElementTree.parse(file)
    root = tree.getroot()
    controls = []
    events = []

    for c in root.findall(".//object[@id]"):
        name = c.get("id")
        ctrl = Control(name, c.get("class")) 
        controls.append(ctrl)
        for x in c.findall("signal"):
            event = Event(ctrl, x.get("name"), x.get("handler"))
            events.append(event)

    header_templ = Template(filename="events.hpp.mako")    
    src_templ = Template(filename="events.cc.mako")    
    with open("./events.hpp", "w") as header:
        header.write(header_templ.render(guard="EVENTS_HPP", controls=controls, events=events))
    print("header")
    with open("./events.cc", "w") as src:
        src.write(src_templ.render(header_path="events.hpp", controls=controls, events=events))




if __name__ == "__main__":
    main()





