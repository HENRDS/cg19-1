
#ifndef EVENTS_HPP
#define EVENTS_HPP
#include <gtk/gtk.h>

namespace CG { namespace ui {
class Context {
    Context();
public:
    GtkEntry* entry_add_name;
    GtkEntry* entry_add_pointx;
    GtkEntry* entry_add_pointy;
    GtkWindow* add_line_window;
    GtkEntry* entry_add_line_name;
    GtkEntry* entry_add_line_x;
    GtkEntry* entry_add_line_y;
    GtkEntry* entry_add_line_x2;
    GtkEntry* entry_add_line_y2;
    GtkButton* btn_add_line;
    GtkWindow* add_object_3D;
    GtkBox* box49;
    GtkBox* box50;
    GtkLabel* label32;
    GtkEntry* entry_add_object_name;
    GtkBox* box51;
    GtkLabel* label33;
    GtkEntry* entry_add_objectx;
    GtkBox* box52;
    GtkLabel* label34;
    GtkEntry* entry_add_objecty;
    GtkBox* box54;
    GtkLabel* label35;
    GtkEntry* entry_add_objectz;
    GtkBox* box53;
    GtkButton* btn_add_vertice;
    GtkButton* btn_finish_object;
    GtkWindow* add_polygon_window;
    GtkEntry* entry_add_polygon_name;
    GtkEntry* entry_add_polygonx;
    GtkEntry* entry_add_polygony;
    GtkEntry* entry_add_polygonz;
    GtkButton* btn_add_polygon;
    GtkWindow* escalonate_window1;
    GtkEntry* entry_escalex;
    GtkEntry* entry_escaley;
    GtkButton* btn_add_escale;
    GtkWindow* includeObject_window;
    GtkButton* btn_include_point;
    GtkButton* btn_include_polygon;
    GtkButton* btn_include_line;
    GtkButton* btn_include_spline;
    GtkButton* btn_include_object3d;
    GtkButton* btn_include_bezier;
    GtkWindow* main_window;
    GtkButton* btn_add_object;
    GtkButton* btn_up;
    GtkButton* btn_left;
    GtkButton* btn_right;
    GtkButton* btn_down;
    GtkComboBoxText* comboboxtext_options;
    GtkButton* btn_zoom_out;
    GtkButton* btn_zoom_in;
    GtkButton* btn_left_rotate;
    GtkButton* btn_right_rotate;
    GtkButton* btn_rotate_object;
    GtkButton* btn_rotate_world;
    GtkButton* btn_rotate_specific;
    GtkButton* btn_escalonate;
    GtkButton* btn_translate;
    GtkWindow* translate_window;
    GtkEntry* entry_translatex;
    GtkEntry* entry_translatey;
    GtkButton* btn_apply_translation;
}

static void on_btn_add_line_activate();
static void on_btn_add_vertice_activate();
static void on_btn_finish_object_activate();
static void on_btn_add_polygon_activate();
static void on_btn_add_escale_activate();
static void on_btn_include_point_activate();
static void on_btn_include_polygon_activate();
static void on_btn_include_line_activate();
static void on_btn_include_spline_activate();
static void on_btn_include_object3d_activate();
static void on_btn_include_bezier_activate();
static void on_btn_add_object_activate();
static void on_btn_up_activate();
static void on_btn_left_activate();
static void on_btn_right_activate();
static void on_btn_down_activate();
static void on_btn_zoom_out_activate();
static void on_btn_zoom_in_activate();
static void on_btn_left_rotate_activate();
static void on_btn_right_rotate_activate();
static void on_btn_rotate_object_activate();
static void on_btn_rotate_world_activate();
static void on_btn_rotate_specific_activate();
static void on_btn_escalonate_activate();
static void on_btn_translate_activate();
static void on_btn_apply_translation_clicked();

}}
#endif // EVENTS_HPP


