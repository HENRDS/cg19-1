#include "${header_path}"

using namespace CG::ui;

Context::Context(GtkBuilder* builder): 
%for control in controls:
    ${control.name}(GTK_WIDGET(gtk_builder_get_object(builder, "${control.name}")))\
%if loop.last:

{}
%else:
,
%endif
%endfor