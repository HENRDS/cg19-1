#include "events.hpp"

using namespace CG::ui;

Context::Context(GtkBuilder* builder): 
    entry_add_name(GTK_WIDGET(gtk_builder_get_object(builder, "entry_add_name"))),
    entry_add_pointx(GTK_WIDGET(gtk_builder_get_object(builder, "entry_add_pointx"))),
    entry_add_pointy(GTK_WIDGET(gtk_builder_get_object(builder, "entry_add_pointy"))),
    add_line_window(GTK_WIDGET(gtk_builder_get_object(builder, "add_line_window"))),
    entry_add_line_name(GTK_WIDGET(gtk_builder_get_object(builder, "entry_add_line_name"))),
    entry_add_line_x(GTK_WIDGET(gtk_builder_get_object(builder, "entry_add_line_x"))),
    entry_add_line_y(GTK_WIDGET(gtk_builder_get_object(builder, "entry_add_line_y"))),
    entry_add_line_x2(GTK_WIDGET(gtk_builder_get_object(builder, "entry_add_line_x2"))),
    entry_add_line_y2(GTK_WIDGET(gtk_builder_get_object(builder, "entry_add_line_y2"))),
    btn_add_line(GTK_WIDGET(gtk_builder_get_object(builder, "btn_add_line"))),
    add_object_3D(GTK_WIDGET(gtk_builder_get_object(builder, "add_object_3D"))),
    box49(GTK_WIDGET(gtk_builder_get_object(builder, "box49"))),
    box50(GTK_WIDGET(gtk_builder_get_object(builder, "box50"))),
    label32(GTK_WIDGET(gtk_builder_get_object(builder, "label32"))),
    entry_add_object_name(GTK_WIDGET(gtk_builder_get_object(builder, "entry_add_object_name"))),
    box51(GTK_WIDGET(gtk_builder_get_object(builder, "box51"))),
    label33(GTK_WIDGET(gtk_builder_get_object(builder, "label33"))),
    entry_add_objectx(GTK_WIDGET(gtk_builder_get_object(builder, "entry_add_objectx"))),
    box52(GTK_WIDGET(gtk_builder_get_object(builder, "box52"))),
    label34(GTK_WIDGET(gtk_builder_get_object(builder, "label34"))),
    entry_add_objecty(GTK_WIDGET(gtk_builder_get_object(builder, "entry_add_objecty"))),
    box54(GTK_WIDGET(gtk_builder_get_object(builder, "box54"))),
    label35(GTK_WIDGET(gtk_builder_get_object(builder, "label35"))),
    entry_add_objectz(GTK_WIDGET(gtk_builder_get_object(builder, "entry_add_objectz"))),
    box53(GTK_WIDGET(gtk_builder_get_object(builder, "box53"))),
    btn_add_vertice(GTK_WIDGET(gtk_builder_get_object(builder, "btn_add_vertice"))),
    btn_finish_object(GTK_WIDGET(gtk_builder_get_object(builder, "btn_finish_object"))),
    add_polygon_window(GTK_WIDGET(gtk_builder_get_object(builder, "add_polygon_window"))),
    entry_add_polygon_name(GTK_WIDGET(gtk_builder_get_object(builder, "entry_add_polygon_name"))),
    entry_add_polygonx(GTK_WIDGET(gtk_builder_get_object(builder, "entry_add_polygonx"))),
    entry_add_polygony(GTK_WIDGET(gtk_builder_get_object(builder, "entry_add_polygony"))),
    entry_add_polygonz(GTK_WIDGET(gtk_builder_get_object(builder, "entry_add_polygonz"))),
    btn_add_polygon(GTK_WIDGET(gtk_builder_get_object(builder, "btn_add_polygon"))),
    escalonate_window1(GTK_WIDGET(gtk_builder_get_object(builder, "escalonate_window1"))),
    entry_escalex(GTK_WIDGET(gtk_builder_get_object(builder, "entry_escalex"))),
    entry_escaley(GTK_WIDGET(gtk_builder_get_object(builder, "entry_escaley"))),
    btn_add_escale(GTK_WIDGET(gtk_builder_get_object(builder, "btn_add_escale"))),
    includeObject_window(GTK_WIDGET(gtk_builder_get_object(builder, "includeObject_window"))),
    btn_include_point(GTK_WIDGET(gtk_builder_get_object(builder, "btn_include_point"))),
    btn_include_polygon(GTK_WIDGET(gtk_builder_get_object(builder, "btn_include_polygon"))),
    btn_include_line(GTK_WIDGET(gtk_builder_get_object(builder, "btn_include_line"))),
    btn_include_spline(GTK_WIDGET(gtk_builder_get_object(builder, "btn_include_spline"))),
    btn_include_object3d(GTK_WIDGET(gtk_builder_get_object(builder, "btn_include_object3d"))),
    btn_include_bezier(GTK_WIDGET(gtk_builder_get_object(builder, "btn_include_bezier"))),
    main_window(GTK_WIDGET(gtk_builder_get_object(builder, "main_window"))),
    btn_add_object(GTK_WIDGET(gtk_builder_get_object(builder, "btn_add_object"))),
    btn_up(GTK_WIDGET(gtk_builder_get_object(builder, "btn_up"))),
    btn_left(GTK_WIDGET(gtk_builder_get_object(builder, "btn_left"))),
    btn_right(GTK_WIDGET(gtk_builder_get_object(builder, "btn_right"))),
    btn_down(GTK_WIDGET(gtk_builder_get_object(builder, "btn_down"))),
    comboboxtext_options(GTK_WIDGET(gtk_builder_get_object(builder, "comboboxtext_options"))),
    btn_zoom_out(GTK_WIDGET(gtk_builder_get_object(builder, "btn_zoom_out"))),
    btn_zoom_in(GTK_WIDGET(gtk_builder_get_object(builder, "btn_zoom_in"))),
    btn_left_rotate(GTK_WIDGET(gtk_builder_get_object(builder, "btn_left_rotate"))),
    btn_right_rotate(GTK_WIDGET(gtk_builder_get_object(builder, "btn_right_rotate"))),
    btn_rotate_object(GTK_WIDGET(gtk_builder_get_object(builder, "btn_rotate_object"))),
    btn_rotate_world(GTK_WIDGET(gtk_builder_get_object(builder, "btn_rotate_world"))),
    btn_rotate_specific(GTK_WIDGET(gtk_builder_get_object(builder, "btn_rotate_specific"))),
    btn_escalonate(GTK_WIDGET(gtk_builder_get_object(builder, "btn_escalonate"))),
    btn_translate(GTK_WIDGET(gtk_builder_get_object(builder, "btn_translate"))),
    translate_window(GTK_WIDGET(gtk_builder_get_object(builder, "translate_window"))),
    entry_translatex(GTK_WIDGET(gtk_builder_get_object(builder, "entry_translatex"))),
    entry_translatey(GTK_WIDGET(gtk_builder_get_object(builder, "entry_translatey"))),
    btn_apply_translation(GTK_WIDGET(gtk_builder_get_object(builder, "btn_apply_translation")))
{}
