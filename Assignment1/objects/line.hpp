//
// Created by henry on 3/19/19.
//

#ifndef ASSIGNMENT1_LINE_HPP
#define ASSIGNMENT1_LINE_HPP
#include "../graphicalObject.hpp"
#include "../geometry/point.hpp"
namespace CG {
class Line: public GraphicalObject {
public:
    void draw(cairo_t* cairo, Viewport* viewport) override;

    Line(const std::string& name, const Point& p1, const Point& p2);
private:
    geometry::Point _p1, _p2;
};
}
#endif //ASSIGNMENT1_LINE_HPP
