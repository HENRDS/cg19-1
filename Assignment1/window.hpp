//
// Created by henry on 3/14/19.
//

#ifndef CLASS1_WINDOW_HPP
#define CLASS1_WINDOW_HPP

#include <gtk/gtk.h>
#include <string>
#include "geometry/point.hpp"
namespace CG {
    struct Size {
    public:
        int width() const;

<<<<<<< HEAD
        int height() const;

        Size(int width, int height);

        Size(const Size &other) = default;

        Size &operator=(const Size &other) = default;

    private:
        int _width, _height;
    };

    class Window {
    public:
        explicit Window(GtkApplication *app);

        void resize(int width, int height);

        void show();

        Window(Point topLeft, Point bottomRight) : _topLeft(topLeft), _bottomRight(bottomRight), step(10) {}

        Window() {}

        // Commands to move the Window

        void moveWindowLeft() {
            _topLeft.x() -= step;
            _bottomRight.x() -= step;
        }

        void moveWindowRight() {
            _topLeft.x() += step;
            _bottomRight.x() += step;
        }

        void moveWindowUp() {
            _topLeft.y() -= step;
            _bottomRight.y -= step;
        }

        void moveWindowDown() {
            _topLeft += step;
            _bottomRight += step;
        }

        //Commands to make zoom in the Window

        void zoomIn() {
            _topLeft.x() -= step;
            _topLeft.y() -= step;

            _bottomRight.x() += step;
            _bottomRight.y() += step;
        }

        void zoomOut() {
            _topLeft.x() += step;
            _topLeft.y() += step;

            _bottomRight.x() -= step;
            _bottomRight.y() -= step;
        }

    private:
        geometry::Point _topLeft, _bottomRight;
        GtkWidget *_window;
        int _width, _height;
        int step;
        //Size _size;
    };
}

=======
namespace CG {
class Window {
public:

    Window(geometry::Point topLeft, geometry::Point bottomRight, size_t step = 10);

    // Commands to move the Window

    void moveWindowLeft();

    void moveWindowRight();

    void moveWindowUp();

    void moveWindowDown();

    //Commands zoom the Window

    void zoomIn();

    void zoomOut();

private:
    geometry::Point _topLeft, _bottomRight;
    size_t _step;
};
}
>>>>>>> a605e2de0954ed02cd699dab43f2f7a8ae838c2c
#endif //CLASS1_WINDOW_HPP
