#include <iostream>
#include "window.hpp"
#include "geometry/point.hpp"
#include "geometry/vector.hpp"
#include "geometry/matrix.hpp"

int runApp(int argc, char* argv[]);
void testVectors();
int main(int argc, char* argv[]) {
    return runApp(argc, argv);
}

void testVectors() {
    using namespace CG::geometry;
    Point p1 {1, 2}, p2(2, 3), p3(10, 23);
    Vector v(p3 - p1);
    std::cout << p3 << " - " << p1 << " = " << v << std::endl;
    Vector u(1, 1);
    u *= 10;
    std::cout << u << (u^v) << std::endl;
    Matrix k {
        {1, 0, 1},
        {0, 1, 0},
        {0, 0, 1}
    };
    std::cout << k;

}
int runApp(int argc, char* argv[]) {
    gtk_init(&argc, &argv);
    GtkBuilder* builder = gtk_builder_new();
    GError* error = nullptr;
    if (gtk_builder_add_from_file(builder, "../main_window.glade", &error) == 0) {
        g_printerr("Error loading file: %s\n", error->message);
        g_clear_error(&error);
        return 1;
    }
    GtkWidget* mainWindow = GTK_WIDGET(gtk_builder_get_object(builder, "main_window"));
    gtk_builder_connect_signals(builder, nullptr);
    g_object_unref(builder);
    gtk_widget_show(mainWindow);
    gtk_main();
    return 0;
}
