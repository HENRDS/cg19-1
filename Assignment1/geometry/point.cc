#include <utility>

//
// Created by henry on 3/14/19.
//

#include "point.hpp"
#include "matrix.hpp"
#include <cmath>
#include <numeric>
using namespace CG::geometry;

Point::Point(const double x, const double y) : _coordinates {x, y} {
}
bool Point::operator==(const Point& rhs) const {
    return std::equal(this->_coordinates.cbegin(), this->_coordinates.cend(), rhs._coordinates.cbegin());
}
bool Point::operator!=(const Point& rhs) const {
    return !(rhs == *this);
}
std::size_t Point::dimensions() const {
    return this->_coordinates.size();
}
Point::Point(double x, double y, double z) : _coordinates {x, y, z} {

}
double Point::x() const {
    return this->_coordinates[0];
}
double Point::y() const {
    return this->_coordinates[1];
}
double Point::z() const {
    return this->_coordinates[2];
}
std::vector<double>::iterator Point::begin() {
    return this->_coordinates.begin();
}
std::vector<double>::const_iterator Point::cbegin() const {
    return this->_coordinates.cbegin();
}
std::vector<double>::iterator Point::end() {
    return this->_coordinates.end();
}
std::vector<double>::const_iterator Point::cend() const {
    return this->_coordinates.cend();
}
Point::Point(std::size_t dimensions) : _coordinates(dimensions) {
}
double& Point::x() {
    return this->_coordinates[0];
}
double& Point::y() {
    return this->_coordinates[1];
}
double& Point::z() {
    return this->_coordinates[2];
}
void Point::translate(const Matrix& kernel) {
    size_t ndim = this->dimensions();
    if (kernel.side() != ndim) {
        throw std::invalid_argument("Point and kernel dimensions don't match");
    }
    std::vector<double> result(ndim);
    for (int i = 0; i < ndim; ++i) {
        for (int j = 0; j < ndim; ++j) {
            result[i] += this->_coordinates[j] * kernel(j, i);
        }
    }
    this->_coordinates = std::move(result);
}

double Point::operator[](size_t index) const {
    return this->_coordinates[index];
}

Point Point::operator-() const {
    Point p(this->dimensions());
    p.map(*this, std::minus<double>());
    return p;
}

