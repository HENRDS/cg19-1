//
// Created by henry on 3/14/19.
//

#ifndef CLASS1_VECTOR2D_HPP
#define CLASS1_VECTOR2D_HPP
#include <ostream>
#include <numeric>
#include "point.hpp"
namespace CG {namespace geometry {

class Vector : public Point {
public:
    friend std::ostream& operator<<(std::ostream& os, const Vector& d);

    Vector(double x, double y);
    Vector(double x, double y, double z);

    template<class... TS>
    explicit Vector(TS... args): Point(args...) {}
    Vector(const Vector&) = default;
    Vector(Vector&&) noexcept = default;
    Vector& operator=(const Vector&) = default;
    Vector& operator=(Vector&&) noexcept = default;

    double dot(const Vector& rhs) const;
    Vector& operator+=(const Vector& rhs);
    Vector& operator-=(const Vector& rhs);
    Vector& operator*=(double rhs);
    Vector& operator/=(double rhs);
    friend double operator^(const Vector& lhs, const Vector& rhs);
};

inline Vector& operator+(Vector lhs, const Vector& rhs) {
    return lhs += rhs;
}
inline Vector& operator-(Vector lhs, const Vector& rhs) {
    return lhs -= rhs;
}
inline Vector& operator*(Vector lhs, double rhs) {
    return lhs *= rhs;
}
inline Vector& operator*(double lhs, Vector rhs) {
    return rhs *= lhs;
}
inline Vector& operator/(Vector lhs, double rhs) {
    return lhs /= rhs;
}

inline Vector operator-(const Point& lhs, const Point& rhs) {
    Vector v(lhs.dimensions());
    std::transform(lhs.cbegin(), lhs.cend(), rhs.cbegin(), v.begin(), std::minus<double>());
    return v;
}
std::ostream& operator<<(std::ostream& os, const Vector& d);
double operator^(const Vector& lhs, const Vector& rhs);
}
}
#endif //CLASS1_VECTOR2D_HPP
