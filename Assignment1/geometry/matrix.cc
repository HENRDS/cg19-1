//
// Created by henry on 3/19/19.
//
#include <iterator>
#include "matrix.hpp"
#include "vector.hpp"
#include <numeric>
#include <exception>
#include <cmath>
using namespace CG::geometry;
Matrix::Matrix(std::initializer_list<std::initializer_list<double>> lines) : _data() {
    size_t nlines = lines.size();
    this->_side = nlines;
    for (const auto& line: lines) {
        if (line.size() != nlines) {
            throw std::invalid_argument("A kernel must be square");
        }
        for (auto val: line) {
            this->_data.push_back(val);
        }
    }
}
std::ostream& CG::geometry::operator<<(std::ostream& os, const Matrix& kernel) {
    return os;
}
std::size_t Matrix::side() const {
    return this->_side;
}
Matrix& Matrix::operator*=(const Matrix& rhs) {
    if (rhs._side != this->_side) {
        throw std::invalid_argument("sizes don't match");
    }
    std::vector<double> data(this->_side);
    for (int k = 0; k < this->_side; ++k) {
        for (int j = 0; j < this->_side; ++j) {
            for (int i = 0; i < this->_side; ++i) {
                data[this->_side * k + i] += this->_data[this->_side * k + j] * rhs._data[this->_side * j + i];
            }
        }
    }
    this->_data = std::move(data);
    return *this;
}
double Matrix::operator()(size_t line, size_t column) const {
    return this->_data[line * this->_side + column];
}
double& Matrix::operator()(size_t line, size_t column) {
    return this->_data[line * this->_side + column];
}
Matrix::Matrix(std::size_t side) : _side(side), _data(side * side) {
}
Matrix::Matrix(std::initializer_list<Point> points) {
    this->_side = points.size();
    for (const auto& p:  points) {
        if (p.dimensions() != this->_side) throw std::invalid_argument("sizes don't match");
        this->_data.insert(this->_data.end(), p.cbegin(), p.cend());
    }
}
Matrix Matrix::scale(const Vector& des) {
    Matrix kernel(des.dimensions());
    size_t last = des.dimensions() - 1;
    for(int i= 0; i < last; ++i) {
        kernel(i,i) = des[i];
    }
    kernel(last, last) = 1;
    return kernel;
}
Matrix Matrix::translate(const Point& des) {
    Matrix kernel = Matrix::eye(des.dimensions());
    for(int i= 0; i < des.dimensions() - 1; ++i) {
        kernel(kernel._side, i) = des[i];
    }
    return kernel;
}

Matrix Matrix::rotate(double angle) {
    double s = sin(angle), c = cos(angle);
    return Matrix {
        {c, -s, 0},
        {s,  c, 0},
        {0,  0, 1}
    };
}
Matrix Matrix::eye(size_t side) {
    Matrix k(side);
    for (int i = 0; i < side; ++i)
        k(i, i) = 1.0;
    return k;
}



