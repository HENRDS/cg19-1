//
// Created by henry on 3/19/19.
//

#ifndef ASSIGNMENT1_MATRIX_HPP
#define ASSIGNMENT1_MATRIX_HPP
#include <cstdint>
#include <initializer_list>
#include <vector>
#include <stdexcept>
#include <numeric>
#include <ostream>
#include "base.hpp"
namespace CG {
namespace geometry {
class Matrix {
public:
    friend std::ostream& operator<<(std::ostream& os, const Matrix& kernel);
    Matrix(std::initializer_list<std::initializer_list<double>> lines);
    Matrix(std::initializer_list<Point> points);
    explicit Matrix(std::size_t side);
    std::size_t side() const;
    Matrix& operator*=(const Matrix& rhs);
    double operator()(size_t line, size_t column) const;
    double& operator()(size_t line, size_t column);
    static Matrix scale(const Vector& des);
    static Matrix translate(const Point& des);
    static Matrix rotate(double angle);
    static Matrix eye(size_t side);
private:
    std::size_t _side;
    std::vector<double> _data;
};
std::ostream& operator<<(std::ostream& os, const Matrix& kernel);
}
}
#endif //ASSIGNMENT1_MATRIX_HPP
