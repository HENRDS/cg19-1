//
// Created by henry on 3/25/19.
//

#ifndef ASSIGNMENT1_RECT_HPP
#define ASSIGNMENT1_RECT_HPP

#include "vector.hpp"
namespace CG {
namespace geometry {
class Rect {
public:
    Rect(double left, double top, double width, double height);
    Rect(Point topLeft, Vector size);
    double top() const;
    double left() const;
    double height() const;
    double width() const;

protected:
    Point _topLeft;
    Vector _size;
};
}
}
#endif //ASSIGNMENT1_RECT_HPP
