//
// Created by henry on 3/14/19.
//

#ifndef ASSIGNMENT1_POINT_HPP
#define ASSIGNMENT1_POINT_HPP
#include <iterator>
#include <vector>
#include <array>
#include <type_traits>
#include <algorithm>
#include "base.hpp"
namespace CG {
namespace geometry {

class Point {
public:
    bool operator==(const Point& rhs) const;
    bool operator!=(const Point& rhs) const;
    friend std::ostream& operator<<(std::ostream& os, const Point& d) {
        os << "Point(";
        std::copy(d._coordinates.cbegin(), d._coordinates.cend(), std::ostream_iterator<double>(os, ", "));
        if (d.dimensions() > 0) os << "\b\b";
        os << ")";
        return os;
    }
    explicit Point(std::size_t dimensions);
    Point(double x, double y);
    Point(double x, double y, double z);
    template<class T, class U, class... TS>
    explicit Point(T x, U y, TS... args): _coordinates {double(x), double(y), double(args)...} {}
    Point(std::initializer_list<double> list) : _coordinates(list) {}

    Point(const Point&) = default;
    Point(Point&&) noexcept = default;
    Point operator-() const;
    Point& operator=(const Point&) = default;
    Point& operator=(Point&&) noexcept = default;
    double operator[](size_t index) const;
    std::vector<double>::iterator begin();
    std::vector<double>::const_iterator cbegin() const;
    std::vector<double>::iterator end();
    std::vector<double>::const_iterator cend() const;


    double x() const;
    double y() const;
    double z() const;

    double& x();
    double& y();
    double& z();


    std::size_t dimensions() const;
    void translate(const Matrix& kernel);
protected:
    std::vector<double> _coordinates;
    template<class UnaryOp>
    void map(UnaryOp op) {
        std::transform(this->_coordinates.cbegin(), this->_coordinates.cend(), this->_coordinates.begin(), op);
    }

    template<class BinaryOp>
    void map(const Point& other, BinaryOp op) {
        std::transform(this->_coordinates.cbegin(),
                       this->_coordinates.cend(),
                       other._coordinates.cbegin(),
                       this->_coordinates.begin(),
                       op
        );
    }
};
}
}

#endif //ASSIGNMENT1_POINT_HPP
