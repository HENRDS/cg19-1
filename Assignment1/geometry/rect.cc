#include <utility>

#include <utility>

//
// Created by henry on 3/25/19.
//

#include "rect.hpp"
CG::geometry::Rect::Rect(CG::geometry::Point topLeft, CG::geometry::Vector size) :
    _topLeft(std::move(topLeft)), _size(std::move(size)) {}
CG::geometry::Rect::Rect(double left, double top, double width, double height) :
    _topLeft(left, top), _size(width, height) {

}
double CG::geometry::Rect::top() const {
    return this->_topLeft.y();
}
double CG::geometry::Rect::left() const {
    return this->_topLeft.x();
}
double CG::geometry::Rect::height() const {
    return this->_size.y();
}
double CG::geometry::Rect::width() const {
    return this->_size.x();
}
