//
// Created by henry on 3/14/19.
//

#include "vector.hpp"
#include <cmath>
#include <iterator>
#include <algorithm>
#include <numeric>
#include <functional>
using namespace CG::geometry;

Vector::Vector(double x, double y) : Point(x, y) {}
Vector::Vector(double x, double y, double z) : Point(x, y, z) {}
std::ostream& CG::geometry::operator<<(std::ostream& os, const Vector& d) {
    os << "Vector(";
    std::copy(d._coordinates.cbegin(), d._coordinates.cend(), std::ostream_iterator<double>(os, ", "));
    if (d.dimensions() > 0) os << "\b\b";
    os << ")";
    return os;
}
double Vector::dot(const Vector& rhs) const {
    return std::inner_product(this->_coordinates.cbegin(), this->_coordinates.cend(), rhs._coordinates.cbegin(), 0.0);
}
Vector& Vector::operator+=(const Vector& rhs) {
    this->map(rhs, std::plus<double>());
    return *this;
}
Vector& Vector::operator-=(const Vector& rhs) {
    this->map(rhs, std::minus<double>());
    return *this;
}
Vector& Vector::operator*=(double rhs) {
    this->map([rhs](double x) { return x * rhs; });
    return *this;
}
Vector& Vector::operator/=(double rhs) {
    this->map([rhs](double x) { return x / rhs; });
    return *this;
}
double CG::geometry::operator^(const Vector& lhs, const Vector& rhs) {
    return lhs.dot(rhs);
}

