//
// Created by henry on 3/25/19.
//

#ifndef ASSIGNMENT1_BASE_HPP
#define ASSIGNMENT1_BASE_HPP
namespace CG {namespace geometry {
    /* Forward declarations */
    class Point;
    class Matrix;
    class Rect;
    class Vector;
}
}
#endif //ASSIGNMENT1_BASE_HPP
