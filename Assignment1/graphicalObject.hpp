//
// Created by henry on 3/14/19.
//
#include <gtk/gtk.h>
#include <string>
#include <utility>
#include <iterator>

#include "geometry/point.hpp"
#include "viewport.hpp"

#ifndef CLASS1_GRAPHICALOBJECT_HPP
#define CLASS1_GRAPHICALOBJECT_HPP
namespace CG {


class Object {
public:
    explicit Object(std::string name) : _name(std::move(name)) {}
    const std::string& name() const {
        return _name;
    }
    void name(const std::string& name) {
        _name = name;
    }
private:
    std::string _name;
};

using Point = geometry::Point;

class GraphicalObject : public Object {
public:
    explicit GraphicalObject(const std::string& name);
    virtual void draw(cairo_t* cairo, Viewport* viewport) = 0;
    virtual Point center() const = 0;
};

}

#endif //CLASS1_GRAPHICALOBJECT_HPP
